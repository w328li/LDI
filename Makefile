.PHONY: all clean
#CFLAGS = `pkg-config --cflags glib-2.0`
#LDLIBS = `pkg-config --libs glib-2.0`
all: SQLPParser Parser

SQLPParser: SQLPParser.c SQLPGrammar.y SQLPScanner.l util.o
	bison --verbose -d SQLPGrammar.y
	flex SQLPScanner.l
	gcc -w SQLPParser.c util.o -o SQLPParser
	rm -f lex.yy.c SQLPGrammar.tab.c SQLPGrammar.tab.h

SQLPParser_new: SQLPParser.c SQLPGrammar_new.y SQLPScanner.l util.o
    bison --verbose -d SQLPGrammar_new.y
    flex SQLPScanner_new.l
    gcc -w SQLPParser.c util.o -o SQLPParser_new
    # rm -f lex.yy.c SQLPGrammar.tab.c SQLPGrammar.tab.h

util.o: util.c util.h
	gcc -c util.c
#	gcc `pkg-config --cflags --libs glib-2.0` util.c
	#gcc -c util.c

Parser: parser.c util.o
	gcc -w parser.c util.o -o Parser

clean:
	rm -f lex.yy.c *.tab.c *.tab.h *.fasl Parser util.o


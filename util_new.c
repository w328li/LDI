#include "util_new.h"

cons_cell* create_cons_cell(void* car, cons_cell* cdr) {
	cons_cell* new_cons = malloc(sizeof(cons_cell));
	new_cons->car = car;
	new_cons->cdr = cdr;
	new_cons->is_atom = false;
	return new_cons;
}


atom* create_atom(char* val) {
	printf("create_atom: %s\n", val);
	atom* new_atom = malloc(sizeof(atom));
	new_atom->val = malloc(sizeof(char) * (strlen(val) + 1));
	strcpy(new_atom->val, val);
	return new_atom;
}


cons_cell* create_cons_cell_w_atom(char* val, cons_cell* cdr) {
	atom* new_atom = create_atom(val);
	cons_cell* new_cons = malloc(sizeof(cons_cell));
	new_cons->car = new_atom;
	new_cons->cdr = cdr;
	new_cons->is_atom = true;
	return new_cons;
}

cons_cell* create_spcol(cons_cell* var, cons_cell* pf) {
	cons_cell* var_cons = create_cons_cell(var, pf);
	char operator[6] = "SPCOL\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, var_cons);
	return operator_cons;
}

cons_cell* create_pf(cons_cell* attr, cons_cell* next_attr) {
	cons_cell* attr_cons = create_cons_cell(attr, next_attr);
	char operator[3] = "PF\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, attr_cons);
	return operator_cons;
}

cons_cell* create_table(char *table) {
	cons_cell* table_cons = create_cons_cell_w_atom(table, NULL);
	char operator[6] = "TABLE\0";
	return create_cons_cell_w_atom(operator, table_cons);
}

cons_cell* create_term(cons_cell* term) {
	cons_cell* term_cons = create_cons_cell(term, NULL);
	char operator[5] = "TERM\0";
	return create_cons_cell_w_atom(operator, term_cons);
}

cons_cell* create_constant(char* constant) {
	cons_cell* constant_cons = create_cons_cell_w_atom(constant, NULL);
	char operator[6] = "CONST\0";
	return create_cons_cell_w_atom(operator, constant_cons);
}

cons_cell* create_col(char *col) {
	cons_cell* col_cons = create_cons_cell_w_atom(col, NULL);
	char operator[4] = "COL\0";
	return create_cons_cell_w_atom(operator, col_cons);
}

cons_cell* create_attr(char *attr) {
	cons_cell* attr_cons = create_cons_cell_w_atom(attr, NULL);
	char operator[5] = "ATTR\0";
	return create_cons_cell_w_atom(operator, attr_cons);
}

cons_cell* create_var(char *var) {
	cons_cell* var_cons = create_cons_cell_w_atom(var, NULL);
	char operator[4] = "VAR\0";
	return create_cons_cell_w_atom(operator, var_cons);
}

cons_cell* create_op(char *op) {
	cons_cell* op_cons = create_cons_cell_w_atom(op, NULL);
	char operator[3] = "OP\0";
	return create_cons_cell_w_atom(operator, op_cons);
}

cons_cell* create_comp_operator(cons_cell* op, cons_cell* term1, cons_cell* term2) {
	cons_cell* term2_cons = create_cons_cell(term2, NULL);
	cons_cell* term1_cons = create_cons_cell(term1, term2_cons);
	cons_cell* op_cons = create_cons_cell(op, term1_cons);
	char operator[5] = "COMP\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, op_cons);
	return operator_cons;
}


cons_cell* create_atom_operator(cons_cell* table, cons_cell* var) {
	cons_cell* var_cons = create_cons_cell(var, NULL);
	cons_cell* tab_name_cons = create_cons_cell(table, var_cons);
	char operator[5] = "ATOM\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, tab_name_cons);
	return operator_cons;
}

cons_cell* create_union_all_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[10] = "UNION_ALL\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_union_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "UNION\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_and_operator(cons_cell* ra1, cons_cell* ra2) {
	printf("create_and_operator called \n");
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[4] = "AND\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_or_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[3] = "OR\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_njoin_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "NJOIN\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_cross_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "CROSS\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_rename_operator(cons_cell* table, cons_cell* var) {
	cons_cell* original_cons = create_cons_cell(table, var);
	char operator[7] = "RENAME\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, original_cons);
	return operator_cons;
}

cons_cell* create_proj_operator(cons_cell* assign, cons_cell* ra) {
	cons_cell* ra_cons = create_cons_cell(ra, NULL);
	cons_cell* assign_cons = create_cons_cell(assign, ra_cons);
	char operator[5] = "PROJ\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, assign_cons);
	return operator_cons;
}

cons_cell* create_not_operator(cons_cell* ra) {
	cons_cell* ra_cons = create_cons_cell(ra, NULL);
	char operator[4] = "NOT\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra_cons);
	return operator_cons;
}

cons_cell* create_exist_operator(cons_cell* ra1) {
	cons_cell* ra1_cons = create_cons_cell(ra1, NULL);
	char operator[6] = "EXIST\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return create_cons_cell_w_atom(operator, ra1_cons);
}

// todo: ra2 number doesn't apprear in the parse tree
cons_cell* create_limit_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[6] = "LIMIT\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_elim_operator(cons_cell* ra1) {
	cons_cell* ra1_cons = create_cons_cell(ra1, NULL);
	char operator[5] = "ELIM\0";
	cons_cell* operator_cons = create_cons_cell_w_atom(operator, ra1_cons);
	return operator_cons;
}

cons_cell* create_eval_operator(cons_cell* logic, cons_cell* cross) {
	cons_cell* cross_cons = create_cons_cell(cross, NULL);
	cons_cell* var_cons = create_cons_cell(logic, cross_cons);
	char operator[5] = "EVAL\0";
	return create_cons_cell_w_atom(operator, var_cons);
}

cons_cell* create_as_operator(cons_cell* ra1, cons_cell* ra2) {
	cons_cell* ra2_cons = create_cons_cell(ra2, NULL);
	cons_cell* ra1_cons = create_cons_cell(ra1, ra2_cons);
	char operator[3] = "AS\0";
	return create_cons_cell_w_atom(operator, ra1_cons);
}

void print_cons_tree(cons_cell *root) {
	print_cons_tree_helper(root, 0);
}

void print_cons_tree_helper(cons_cell *root, int indent) {
	if (root == NULL) {
		return;
	}

	bool tmp_atom = root->is_atom;
	if (root->is_atom) {
		for (int i = 0; i < indent; i++) {
			printf("  ");
		}
		printf("%s", ((atom*)root->car)->val);
		printf("\n");
		print_cons_tree_helper(root->cdr, indent+1);
	} else {
		cons_cell* tmp = (cons_cell*)root->car;
		print_cons_tree_helper((cons_cell*)root->car, indent);
		print_cons_tree_helper(root->cdr, indent);
	}
}


%{
	#include "util.h"
%}

%define api.value.type {cons_cell *}
%token IMPLIES OR AND NOT LE GE LT GT NE HAS MAX MIN AS ASC DESC MOD ASSIGN EQ STAR COMMA DOT

%token SIZE SELECTIVITY OVERLAP
%token FREQUENCY UNIT TIME SPACE

%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF

%token STORE STORING DYNAMIC STATIC OF TYPE ORDERED BY
%token INDEX LIST ARRAY BINARY TREE DISTRIBUTED POINTER

%token SCHEMA  CLASS ISA PROPERTIES CONSTRAINTS PROPERTY
%token ON DETERMINED COVER QUERY GIVEN FROM SELECT WHERE ORDER
%token PRECOMPUTED ONE EXIST FOR ALL TRANSACTION INTCLASS STRCLASS
%token INTEGER REAL DOUBLEREAL STRING MAXLEN RANGE TO
%token INSERT END CHANGE DELETE DECLARE RETURN UNION

%start SQLPProgram
%%
SQLPProgram
    : Query
        { printf("Input Query\n");
          cons_cell *n = $1;
          printf("Printing Tree\n");
          print_cons_tree(n);
        }
    ;

Query
	: Union_Query
		{ printf("Union Query\n");
		  $$ = $1;
		}
	;

Select_Query
	: SELECT Select_List Body
		{ printf("SQLP Query\n");
		  $$ = create_proj_operator($2, $3);
		}
	;

Body
	: FROM TablePath
		{ printf("Body 1\n");
		  $$ =  $2;
		}
	| FROM TablePath WHERE Bool
		{ printf("Body 2\n");
		  $$ = create_eval_operator($4, $2);
        }
    | FROM TablePath WHERE Pred
		{ printf("Body 3\n");
		  $$ = create_eval_operator($4, $2);
        }
	;

TablePath
    : TableIdentifier VarIdentifier
        { printf("last table path\n");
          $$ = create_rename_operator($1, $2);
        }
    | TableIdentifier VarIdentifier ',' TablePath
        { printf("table path2\n");
          $$ = create_cross_operator(create_rename_operator($1, $2), $4);
    	}
    ;

Union_Query
   : Select_Query
        { printf("union query 1\n");
          $$ = $1;
      	}
   | Union_Query UNION Select_Query
   		{ printf("union query 2\n");
   		  $$ = create_union_all_operator($1, $3);
        }
   ;

Select_List
	: STAR
		{ printf("star\n");
		  //$$ = new_node(1, Select_List);
		  //$$->children[0] = new_node(0, STAR);
		}
	| Col
		{ printf("select list attr path\n");
		  $$ = create_rename_operator($1, NULL);
      	}
	| Col ',' Select_List
		{ printf("Select list\n");
		  $$ = create_rename_operator($1, $2);
		}
	;

Col
    : VarIdentifier '.' AttrPath
        {
            printf("col\n");
            $$ = create_spcol($1, $3);
        }

AttrPath
	: AttrIdentifier
		{ printf("path id\n");
		  $$ = create_pf($1, NULL);
		}
	| AttrIdentifier '.' AttrPath
		{ printf("Path Function\n");
		  $$ = create_pf($1, $3);
		}
	;

VarIdentifier
		: IDENTIFIER
				{
				  printf("|%s| ", yytext);
					$$ = create_var(yytext);
				}

TableIdentifier
		: IDENTIFIER
				{
				  printf("|%s| ", yytext);
					$$ = create_table(yytext);
				}

AttrIdentifier
		: IDENTIFIER
			{
				printf("|%s| ", yytext);
				$$ = create_attr(yytext);
			}

Operator
	: EQ
		{
			$$ = create_op(yytext);
		}
	| NE
		{
			$$ = create_op(yytext);
		}
	| LE
		{
			$$ = create_op(yytext);
		}
	| GE
		{
			$$ = create_op(yytext);
		}
	| LT
		{
			$$ = create_op(yytext);
		}
	| GT
		{
			$$ = create_op(yytext);
		}
	;

Bool
	: Col Operator Col
		{ printf("Col op Col\n");
			cons_cell* first_term = create_term($1);
			cons_cell* second_term = create_term($3);
		  $$ = create_comp_operator($2, first_term, second_term, NULL);
		}
	| Col Operator Constant
		{ printf("Col op Constant\n");
			cons_cell* first_term = create_term($1);
		  $$ = create_comp_operator($2, first_term, $3, NULL);
		}
	;

Pred
	: Bool AND Pred
		{ printf("pred and pred\n");
          $$ = $1;
          $$->cdr->cdr->cdr->cdr->car = $3;
		}
	| NOT Pred
		{ printf("Not Pred\n");
		  $$ = create_not_operator($1, NULL);
		}
	| EXIST '(' Query ')'
		{ printf("Exist query\n");
		  $$ = create_atom("INPROG");
		  //$$->children[0] = new_node(0, EXIST);
		  //$$->children[1] = $3;
		}
	;

Constant
	: INTEGER
		{ printf("|%s|", yytext);
			$$ = create_constant(yytext);
		}
	| REAL
		{ printf("|%s|", yytext);
			$$ = create_constant(yytext);
		}
	| STRING
		{ printf("|%s|", yytext);
			$$ = create_constant(yytext);
		}

